package mu.karier.konsultasikecantikan.ui.tanyadokter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import mu.karier.konsultasikecantikan.databinding.DialogPreConsulBinding
import mu.karier.konsultasikecantikan.getNamePreference

interface PreConsulListener {
    fun onNext(info: String)
}

class PreConsulDialogFragment : BottomSheetDialogFragment() {

    private var _binding: DialogPreConsulBinding? = null
    private val binding get() = _binding!!

    var listener: PreConsulListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogPreConsulBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnNext.setOnClickListener {
            dismiss()
            listener?.onNext(getInformation())
        }
    }

    private fun getInformation(): String {
        val name = requireContext().getNamePreference()
        val gender =
            view?.findViewById<RadioButton>(binding.rgGender.checkedRadioButtonId)?.text.toString()
        val problem =
            view?.findViewById<RadioButton>(binding.rgProblem.checkedRadioButtonId)?.text.toString()
        return "Halo dokter, nama saya $name. Saya seorang $gender, saya memiliki masalah wajah $problem. Apa yang harus saya lakukan untuk mengatasi masalah wajah saya ya dok?"
    }
}