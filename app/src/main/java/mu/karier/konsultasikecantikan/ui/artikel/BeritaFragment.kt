package mu.karier.konsultasikecantikan.ui.artikel

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import mu.karier.konsultasikecantikan.R
import mu.karier.konsultasikecantikan.data.Artikel
import mu.karier.konsultasikecantikan.ui.artikel.placeholder.PlaceholderContent
import mu.karier.kuisbahasa.extension.openChromeBrowser

/**
 * A fragment representing a list of Items.
 */
class BeritaFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_berita_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = LinearLayoutManager(context)
                adapter = BeritaRecyclerViewAdapter(Artikel.fetch(requireContext()))
                (adapter as BeritaRecyclerViewAdapter).mClickListener =
                    object : BeritaRecyclerViewAdapter.BeritaListener {
                        override fun onClick(item: Artikel) {
                            findNavController().navigate(BeritaFragmentDirections.actionNavigateHomeToWebview(item.url))
                        }
                    }
            }
        }
        return view
    }

}