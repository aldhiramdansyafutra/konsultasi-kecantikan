package mu.karier.konsultasikecantikan.ui.produk

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import mu.karier.konsultasikecantikan.R
import mu.karier.konsultasikecantikan.data.Produk

/**
 * A fragment representing a list of Items.
 */
class ProdukFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_produk_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = LinearLayoutManager(context)
                adapter = ProdukRecyclerViewAdapter(Produk.fetch(requireContext()))
                (adapter as ProdukRecyclerViewAdapter).mOnClickListener =
                    object : ProdukRecyclerViewAdapter.ProdukListener {
                        override fun onClick(item: Produk) {
                            findNavController().navigate(
                                ProdukFragmentDirections.actionNavigateProdukListToDetail(item)
                            )
                        }
                    }
            }
        }
        return view
    }
}