package mu.karier.konsultasikecantikan.ui.tanyadokter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mu.karier.konsultasikecantikan.data.Dokter
import mu.karier.konsultasikecantikan.databinding.ItemDokterBinding

class DokterRecyclerViewAdapter(
    private val values: List<Dokter>
) : RecyclerView.Adapter<DokterRecyclerViewAdapter.ViewHolder>() {

    var materiAdapterListener: DokterAdapterListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemDokterBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.binding.tvName.text = item.nama
        holder.binding.tvStudy.text = "Bidang Studi ${item.spezialist}"
        holder.itemView.setOnClickListener { materiAdapterListener?.onClicked(item) }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val binding: ItemDokterBinding) :
        RecyclerView.ViewHolder(binding.root)

}

interface DokterAdapterListener {
    fun onClicked(materi: Dokter)
}