package mu.karier.konsultasikecantikan.ui.welcome

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import mu.karier.konsultasikecantikan.R
import mu.karier.konsultasikecantikan.databinding.FragmentSplashBinding
import mu.karier.konsultasikecantikan.getNamePreference

class SplashFragment : Fragment() {

    private var _binding: FragmentSplashBinding? = null
    private val binding get() = _binding!!

    private val handler = Handler()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSplashBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handler.postDelayed({
            if (requireContext().getNamePreference().isEmpty()) {
                findNavController().navigate(R.id.action_navigate_splash_to_onboard)
            } else {
                findNavController().navigate(R.id.action_navigate_splash_to_main)
                requireActivity().finish()
            }
        }, 3000)
    }
}