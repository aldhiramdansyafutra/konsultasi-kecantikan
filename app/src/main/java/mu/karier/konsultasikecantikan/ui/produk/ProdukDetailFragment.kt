package mu.karier.konsultasikecantikan.ui.produk

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import coil.load
import mu.karier.konsultasikecantikan.data.Produk
import mu.karier.konsultasikecantikan.databinding.FragmentProdukDetailBinding

class ProdukDetailFragment : Fragment() {

    private var _binding: FragmentProdukDetailBinding? = null
    private val binding get() = _binding!!

    private var produk = Produk()

    companion object {
        const val PRODUCT = "produk"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            produk = it.getParcelable(PRODUCT) ?: Produk()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProdukDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            ivProduk.load(produk.gambar)
            tvProdukName.text = produk.nama
            tvKelebihan.text = produk.kelebihan
            tvPenggunaan.text = produk.penggunaan
        }
    }
}