package mu.karier.konsultasikecantikan.ui.welcome

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import mu.karier.konsultasikecantikan.MainActivityArgs
import mu.karier.konsultasikecantikan.R
import mu.karier.konsultasikecantikan.databinding.WelcomeFragmentBinding
import mu.karier.konsultasikecantikan.setNamePreference

class WelcomeFragment : Fragment() {

    private var _binding: WelcomeFragmentBinding? = null
    private val binding get() = _binding!!

    private lateinit var viewModel: WelcomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = WelcomeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(WelcomeViewModel::class.java)
        binding.btnNext.setOnClickListener {
            val text = binding.etUsername.text.toString()
            if (text.isEmpty()) {
                binding.tilUsername.error = "Nama wajib di isi"
            } else {
                requireContext().setNamePreference(text)
                findNavController().navigate(WelcomeFragmentDirections.actionNavigateWelcomeToMain(true))
            }
        }
    }
}