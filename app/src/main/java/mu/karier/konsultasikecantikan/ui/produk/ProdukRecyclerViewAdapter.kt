package mu.karier.konsultasikecantikan.ui.produk

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import mu.karier.konsultasikecantikan.data.Produk
import mu.karier.konsultasikecantikan.databinding.ItemProdukBinding

class ProdukRecyclerViewAdapter(
    private val values: List<Produk>
) : RecyclerView.Adapter<ProdukRecyclerViewAdapter.ViewHolder>() {

    var mOnClickListener: ProdukListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            ItemProdukBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.binding.apply {
            tvProductName.text = item.nama
            ivProduk.load(item.gambar)
        }
        holder.itemView.setOnClickListener {
            mOnClickListener?.onClick(item)
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val binding: ItemProdukBinding) : RecyclerView.ViewHolder(binding.root)

    interface ProdukListener {
        fun onClick(item: Produk)
    }

}