package mu.karier.konsultasikecantikan.ui.permasalahan

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mu.karier.konsultasikecantikan.data.Keluhan
import mu.karier.konsultasikecantikan.databinding.ItemPermasalahanBinding

class PermasalahanRecyclerViewAdapter(
    private val values: List<Keluhan>
) : RecyclerView.Adapter<PermasalahanRecyclerViewAdapter.ViewHolder>() {

    var mClickListener: OnPermasalahanClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            ItemPermasalahanBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.binding.apply {
            tvNumber.text = "${(position + 1)}."
            tvAsk.text = item.nama
        }
        holder.itemView.setOnClickListener {
            mClickListener?.onClick(item)
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val binding: ItemPermasalahanBinding) : RecyclerView.ViewHolder(binding.root)

    interface OnPermasalahanClickListener {
        fun onClick(item: Keluhan)
    }
}