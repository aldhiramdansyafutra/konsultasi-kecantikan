package mu.karier.konsultasikecantikan.ui.permasalahan

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import mu.karier.konsultasikecantikan.data.Keluhan
import mu.karier.konsultasikecantikan.databinding.BottomSheetPermasalahanBinding
import mu.karier.konsultasikecantikan.databinding.FragmentPermasalahanListBinding

/**
 * A fragment representing a list of Items.
 */
class PermasalahanFragment : Fragment() {

    private var _binding: FragmentPermasalahanListBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPermasalahanListBinding.inflate(inflater, container, false)
        with(binding.list) {
            layoutManager = LinearLayoutManager(context)
            adapter = PermasalahanRecyclerViewAdapter(Keluhan.fetch(requireContext()))
        }
        (binding.list.adapter as PermasalahanRecyclerViewAdapter).mClickListener =
            object : PermasalahanRecyclerViewAdapter.OnPermasalahanClickListener {
                override fun onClick(item: Keluhan) {
                    showBottomSheet(item)
                }

            }
        return binding.root
    }

    private fun showBottomSheet(item: Keluhan) {
        val bottomSheet = BottomSheetDialog(requireContext())
        val viewBinding = BottomSheetPermasalahanBinding.inflate(
            layoutInflater, binding.list, false
        )

        viewBinding.apply {
            tvName.text = item.nama
            tvPenyebab.text = item.penyebab
            tvSolusi.text = item.solusi
            tvRekomendasi.text = item.rekomendasi
        }

        bottomSheet.setContentView(viewBinding.root)
        bottomSheet.show()
    }
}