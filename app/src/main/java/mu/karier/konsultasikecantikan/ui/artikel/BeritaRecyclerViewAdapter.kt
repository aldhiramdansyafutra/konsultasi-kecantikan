package mu.karier.konsultasikecantikan.ui.artikel

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.RoundedCornersTransformation
import mu.karier.konsultasikecantikan.data.Artikel
import mu.karier.konsultasikecantikan.databinding.ItemBeritaBinding

class BeritaRecyclerViewAdapter(
    private val values: List<Artikel>
) : RecyclerView.Adapter<BeritaRecyclerViewAdapter.ViewHolder>() {

    var mClickListener: BeritaListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            ItemBeritaBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.binding.apply {
            tvTitle.text = item.title
            ivBerita.load(item.image) {
                transformations(RoundedCornersTransformation(8f))
            }
        }
        holder.itemView.setOnClickListener {
            mClickListener?.onClick(item = item)
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val binding: ItemBeritaBinding) : RecyclerView.ViewHolder(binding.root)

    interface BeritaListener {
        fun onClick(item: Artikel)
    }
}