package mu.karier.konsultasikecantikan.ui.tanyadokter

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import mu.karier.konsultasikecantikan.data.Dokter
import mu.karier.konsultasikecantikan.databinding.BottomSheetCallConfirmationBinding
import mu.karier.konsultasikecantikan.databinding.FragmentDokterBinding
import mu.karier.kuisbahasa.extension.openChromeBrowser
import java.lang.Exception
import java.net.URLEncoder

class TanyaDokterFragment : Fragment() {

    var guruAdapter: DokterRecyclerViewAdapter? = null
    private var _binding: FragmentDokterBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDokterBinding.inflate(inflater, container, false)
        guruAdapter = DokterRecyclerViewAdapter(Dokter.fetch(requireContext()))
        guruAdapter?.materiAdapterListener = object : DokterAdapterListener {
            override fun onClicked(materi: Dokter) {
                showBottomSheet(materi)
            }
        }

        // Set the adapter
        with(binding.list) {
            layoutManager = LinearLayoutManager(context)
            adapter = guruAdapter
        }
        return binding.root
    }

    private fun showBottomSheet(dokter: Dokter) {
        val bottomSheet = BottomSheetDialog(requireContext())
        val viewBinding = BottomSheetCallConfirmationBinding.inflate(
            layoutInflater, binding.container, false
        )
        viewBinding.tvCallTeacher.text = "Hubungi Dokter ${dokter.nama}?"
        viewBinding.btnCancel.setOnClickListener {
            bottomSheet.dismiss()
        }
        viewBinding.btnNext.setOnClickListener {
            bottomSheet.dismiss()
            openConsulDialog(dokter)
        }
        bottomSheet.setContentView(viewBinding.root)
        bottomSheet.show()
    }

    private fun phoneCall(phone: String) {
        val uri = "tel:" + phone.trim()
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse(uri)
        startActivity(intent)
    }

    private fun openWhatsapp(dokter: Dokter, info: String) {
        try {
            val url = "https://api.whatsapp.com/send?phone=${dokter.phone}&text=${
                URLEncoder.encode(info, "UTF-8")
            }"
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_VIEW
            sendIntent.data = Uri.parse(url)
            sendIntent.setPackage("com.whatsapp")
            startActivity(sendIntent)
        } catch (e: Exception) {
            Toast.makeText(
                requireContext(),
                "Kamu tidak memiliki aplikasi whatsapp",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun openConsulDialog(dokter: Dokter) {
        val dialog = PreConsulDialogFragment()
        dialog.listener = object : PreConsulListener {
            override fun onNext(info: String) {
                openWhatsapp(dokter, info)
            }
        }
        dialog.show(childFragmentManager, "")
    }
}