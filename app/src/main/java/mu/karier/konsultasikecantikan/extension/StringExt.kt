package mu.karier.konsultasikecantikan.extension

fun String?.withDefault(default: String = ""): String {
    return if (!isNullOrEmpty()) this else default
}