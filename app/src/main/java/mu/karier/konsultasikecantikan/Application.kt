package mu.karier.konsultasikecantikan

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import mu.karier.konsultasikecantikan.Application.Companion.USERNAME

class Application : Application() {

    lateinit var preference: SharedPreferences

    companion object {
        const val MY_PREFERENCE = "MyPreference"
        const val USERNAME = "username"
    }

    override fun onCreate() {
        super.onCreate()
        preference = getSharedPreferences(MY_PREFERENCE, MODE_PRIVATE)
    }
}

fun Context.getNamePreference(): String {
    val pref = getSharedPreferences(mu.karier.konsultasikecantikan.Application.MY_PREFERENCE, 0)
    return pref.getString(USERNAME, "") ?: ""
}

fun Context.setNamePreference(default: String = "") {
    val pref = getSharedPreferences(mu.karier.konsultasikecantikan.Application.MY_PREFERENCE, 0)
    val edit = pref.edit()
    edit.putString(USERNAME, default)
    edit.apply()
}