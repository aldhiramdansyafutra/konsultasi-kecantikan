package mu.karier.konsultasikecantikan.data

import android.content.Context
import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset

@Parcelize
data class Artikel(
    val id: Int,
    val title: String,
    val image: String,
    val url: String
) : Parcelable {
    companion object {
        fun fetch(context: Context): ArrayList<Artikel> {
            val list: ArrayList<Artikel> = ArrayList()
            try {
                val obj = JSONObject(loadLocationJson(context))
                val data: JSONArray = obj.getJSONArray("data")
                val count = data.length()
                for (i in 0 until count) {
                    list.add(parse(data.getJSONObject(i)))
                }
            } catch (ex: JSONException) {
                ex.printStackTrace()
            }
            return list
        }

        private fun loadLocationJson(context: Context): String {
            var json = ""
            return try {
                val `is`: InputStream = context.assets.open("news.json")
                val size: Int = `is`.available()
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                json = String(buffer, Charset.forName("UTF-8"))
                json
            } catch (ex: IOException) {
                ex.printStackTrace()
                json
            }
        }

        private fun parse(data: JSONObject): Artikel {
            return Artikel(
                id = data.getInt("id"),
                title = data.getString("title"),
                image = data.getString("image"),
                url = data.getString("url")
            )
        }
    }
}