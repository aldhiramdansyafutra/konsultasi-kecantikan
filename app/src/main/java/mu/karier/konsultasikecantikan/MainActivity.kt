package mu.karier.konsultasikecantikan

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import mu.karier.konsultasikecantikan.databinding.ActivityMainBinding
import mu.karier.konsultasikecantikan.databinding.DialogWelcomeBinding
import mu.karier.konsultasikecantikan.extension.withDefault

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarMain.toolbar)

        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_artikel,
                R.id.nav_tanya_dokter,
                R.id.nav_about,
                R.id.nav_permasalahan,
                R.id.nav_produk
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        setupName()
        createDialog()
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun setupName() {
        val header: View? = binding.navView.getHeaderView(0)
        val textView = header?.findViewById<AppCompatTextView>(R.id.tv_username)
        textView?.apply {
            text = getNamePreference().withDefault("Hai Kamu ...")
        }
    }

    private fun createDialog() {
        val builder = AlertDialog.Builder(this)
        // Get the layout inflater
        val inflater = layoutInflater
        val binding = DialogWelcomeBinding.inflate(inflater)

        binding.tvWelcome.text = "Selamat datang ${getNamePreference()}"

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(binding.root)
        builder.setCancelable(true)
        builder.create()

        builder.show()
    }
}